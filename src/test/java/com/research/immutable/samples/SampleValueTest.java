package com.research.immutable.samples;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("My collection of Sample Value Tests")
class SampleValueTest
{
    private SampleValue sampleValue;

    @BeforeEach
    void setUp()
    {
        this.sampleValue = ImmutableSampleValue.builder().myDogs(2).age(49).name("Jason").build();
    }

    @DisplayName("Test to get number of dogs")
    @Test
    void myDogs()
    {
        assertEquals(2, this.sampleValue.myDogs(), "I have two dogs");
    }

    @DisplayName("Test to get my name")
    @Test
    void name()
    {
        assertEquals("Jason", this.sampleValue.name(), "My name is Jason");
    }

    @DisplayName("Test for my age")
    @Test
    void age()
    {
        assertEquals(49, this.sampleValue.age(), "I am 49 years old");
    }
}